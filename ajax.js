function ajax(options){
  options=options||{};
  options.data=options.data||{};
  options.type=options.type||"get";
  options.dataType=options.dataType||"text";

  let xhr=new XMLHttpRequest();
  let arr=[];
  for(let name in options.data){
    arr.push(`${name}=${options[name]}`);
  }
  let strData=arr.join("&");

  if(options.type=="post"){
    xhr.open(options.type,options.url,true);
    xhr.setRequestHeader("content-type","application/x-www-form-urlencoded");
    xhr.send(strData);
  }else{
    xhr.open(options.type,options.url+"?"+strData,true);
    xhr.send();
  }
  xhr.onreadystatechange=function(){
    if(xhr.readyState==4){
      if(xhr.status>=200&&xhr.status<300||xhr.status==304){
        let data=xhr.responseText;
        swich(options.dataType){
          case "JSON":
          if(window.JSON&&JSON.parse){
            data=JSON.parse(data);
          }else{
            data=evel("("+str+")");
          }
          break;
        switch (options.dataType) {
          case "xml":
            data=xhr.responseXML;
            break;
          default:

        }
        }
        options.success||options.success(data);
      }else{
        options.error||options.error();
      }
    }
  }
}
